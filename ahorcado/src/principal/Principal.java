package principal;

import java.io.File;
import java.util.Scanner;

/**
 * @author dverdu
 */
public class Principal {
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private static boolean acertado;
	private static int fallos;

	public static void main(String[] args) {

		String palabraSecreta;
		String[] palabras = cargarPalabras();
		palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];

		char[][] caracteresPalabra = new char[2][];
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];
		Scanner input = new Scanner(System.in);

		String caracteresElegidos = "";

		System.out.println("Acierta la palabra");

		System.out.println("####################################");

		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
		System.out.println();

		System.out.println("Introduce una letra o acierta la palabra");
		System.out.println("Caracteres Elegidos: " + caracteresElegidos);
		caracteresElegidos += input.nextLine().toUpperCase();
		fallos = 0;

		boolean encontrado;
		for (int j = 0; j < caracteresElegidos.length(); j++) {
			encontrado = false;
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
					caracteresPalabra[1][i] = '1';
					encontrado = true;
				}
			}
			if (!encontrado)
				fallos++;
		}

		do {

			System.out.println("Acierta la palabra");

			System.out.println("####################################");

			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[1][i] != '1') {
					System.out.print(" -");
				} else {
					System.out.print(" " + caracteresPalabra[0][i]);
				}
			}
			System.out.println();

			System.out.println("Introduce una letra o acierta la palabra");
			System.out.println("Caracteres Elegidos: " + caracteresElegidos);
			caracteresElegidos += input.nextLine().toUpperCase();
			fallos = 0;

			for (int j = 0; j < caracteresElegidos.length(); j++) {
				encontrado = false;
				for (int i = 0; i < caracteresPalabra[0].length; i++) {
					if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
						caracteresPalabra[1][i] = '1';
						encontrado = true;
					}
				}
				if (!encontrado)
					fallos++;
			}

			menu();

			if (fallos >= FALLOS) {
				System.out.println("Has perdido: " + palabraSecreta);
			}
			acertado = true;
			for (int i = 0; i < caracteresPalabra[1].length; i++) {
				if (caracteresPalabra[1][i] != '1') {
					acertado = false;
					break;
				}
			}
			if (acertado)
				System.out.println("Has Acertado ");
		} while (!acertado && fallos < FALLOS);

		input.close();
	}

	/**
	 * Carga las palabras del fichero
	 * @return array de palabras obetenidas
	 */
	private static String[] cargarPalabras() {
		String ruta = "src\\palabras.txt";
		String[] palabras = new String[NUM_PALABRAS];

		File fichero = new File(ruta);
		Scanner inputFichero = null;
		try {
			inputFichero = new Scanner(fichero);
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = inputFichero.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fichero != null && inputFichero != null)
				inputFichero.close();
		}
		return palabras;

	}
	
	/**
	 * Pinta el menu
	 */
	private static void menu() {
		switch (fallos) {
		case 1:

			System.out.println("     ___");
			break;
		case 2:

			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 6:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println("     ___");
			break;
		case 7:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
			break;
		}

	}

}
