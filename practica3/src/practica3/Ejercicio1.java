
package practica3;

import java.util.Scanner;

/**
 * clase de obtencion de dni
 * 
 * @author dverdu
 * @since 15-03-2018
 */
public class Ejercicio1 {

	final static String INICIO = "Bienvenido al programa";

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);

		// Declaracion de variables
		int numeroA = 7;
		int numeroB = 16;
		int numeroC = 25;
		String dni = "";
		String nombre = "";

		System.out.println(INICIO);
		System.out.println("Introduce tu dni");
		dni = lector.nextLine();
		System.out.println("Introduce tu nombre");
		nombre = lector.nextLine();

		System.out.println("Tu nombre y dni son: " + dni + "y" + nombre);

		if (numeroA > numeroB || numeroC % 5 != 0 &&
				((numeroC * 3) - 1) > (numeroB / numeroC)) {
			System.out.println("Se cumple la condición");
		}

		else {
			System.out.println("No se cumple la condición");
		}

		numeroC = (numeroA + numeroB) * (numeroC + numeroB) / numeroA;

		String array[] = new String[7];
		array[0] = "Lunes";
		array[1] = "Martes";
		array[2] = "Miercoles";
		array[3] = "Jueves";
		array[4] = "Viernes";
		array[5] = "Sabado";
		array[6] = "Domingo";

		recorrerArray(array);

		lector.close();
	}

	/**
	 * Recorre la lista 
	 * @param vectorDeStrings array con los dias de la semana
	 */
	static void recorrerArray(String vectorDeStrings[]) {
		for (int i = 0; i < 7; i++) {
			System.out.println(
					"El dia de la semana en el que te encuentras " + (i + 8) + 
					" es el dia: " + vectorDeStrings[i]);
		}
	}

}