
package practica3;

import java.util.Scanner;

/**
 * Clase de medias
 * @author dverdu
 * @since 15-03-2018
 */
public class Ejercicio2 {
	
	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);
		
		int cantidadMaximaAlumnos = 10;
		int arrays[] = new int[10];
		
		// Recorre las notas medias de los alumnos
		for (int i = 0; i < 10; i++) {
			System.out.println("Introduce nota media de alumno");
			arrays[i] = lector.nextInt();
		}

		System.out.println("El resultado es: " + (recorrerArray(arrays)) 
				/ cantidadMaximaAlumnos);

		lector.close();
	}
	
	/**
	 * Recorre el array
	 * @param vector de las medias
	 * @return numero
	 */
	static double recorrerArray(int vector[]) {
		double numero = 0;
		for (int i = 0; i < 10; i++) {
			numero = numero + vector[i];
		}
		return numero / 10;
	}

}